package com.atguigu.gmall.product;


import com.baomidou.mybatisplus.core.conditions.interfaces.Func;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootTest
public class completableFutureTest {


    @Test
    public void getCompletableFuture() throws ExecutionException, InterruptedException {
            CompletableFuture future = CompletableFuture.supplyAsync(new Supplier<Object>() {
                @Override
                public Object get() {
                    System.out.println(Thread.currentThread().getName() + "\t completableFuture");
                    int i = 10 / 0;
                    return 1024;
                }
            }).whenComplete(new BiConsumer<Object, Throwable>() {
                @Override
                public void accept(Object o, Throwable throwable) {
                    System.out.println("-------o=" + o.toString());
                    System.out.println("-------throwable=" + throwable);
                }
            })
                    .exceptionally(new Function<Throwable, Object>() {
                @Override
                public Object apply(Throwable throwable) {
                    System.out.println("throwable=" + throwable);
                    return 6666;
                }
            })

                    ;
            System.out.println(future.get());
    }

    @Test
    public void getUUIDTest(){
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
    }


}
