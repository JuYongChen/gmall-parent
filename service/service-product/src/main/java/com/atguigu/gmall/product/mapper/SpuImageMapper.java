package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.SpuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * author:atGuiGu-mqx
 * date:2022/8/28 14:41
 * 描述：
 **/
@Mapper
public interface SpuImageMapper extends BaseMapper<SpuImage> {

}
