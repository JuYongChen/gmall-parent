package com.atguigu.gmall.user.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.model.user.UserAddress;
import com.atguigu.gmall.user.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * author:atGuiGu-mqx
 * date:2022/9/13 9:25
 * 描述：
 **/
@RestController
@RequestMapping("/api/user")
public class UserApiController {

    @Autowired
    private UserAddressService userAddressService;



//    服务调用接口
    @GetMapping("/inner/findUserAddressListByUserId/{userId}")
    public List<UserAddress> findUserAddressListByUserId(@PathVariable String userId){
        //  调用服务层方法
        return this.userAddressService.getUserAddressListByUserId(userId);
    }
    //  获取收货地址列表!
    @GetMapping("userAddress/auth/findUserAddressList")
    public Result findUserAddressList(HttpServletRequest request){
        //  获取用户Id
        String userId = AuthContextHolder.getUserId(request);
        //  获取列表
        List<UserAddress> userAddressList = this.userAddressService.getUserAddressListByUserId(userId);
        //  返回收货地址列表
        return Result.ok(userAddressList);
    }

}
