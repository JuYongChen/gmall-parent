package com.atguigu.gmall.task.scheduled;

import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.service.RabbitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * author:atGuiGu-mqx
 * date:2022/9/20 10:37
 * 描述：
 **/
@Component
@EnableScheduling
public class ScheduledTask {

    @Autowired
    private RabbitService rabbitService;

    //  凌晨 1 点钟 发起定时任务.
    //  灵活管理定时任务; xxl-job 分布式定时任务调度框架 https://www.xuxueli.com/xxl-job/  500 块钱！
    @Scheduled(cron = "0/10 * * * * ?")
    public void importToRedis(){
        this.rabbitService.sendMsg(MqConst.EXCHANGE_DIRECT_TASK,MqConst.ROUTING_TASK_1,"hello");
        //  System.out.println("死鬼，来人了，开始接客吧.");
    }
    //  清空缓存数据。
    @Scheduled(cron = "* * 18 * * ?")
    public void clearRedisData(){
        this.rabbitService.sendMsg(MqConst.EXCHANGE_DIRECT_TASK,MqConst.ROUTING_TASK_18,"clear");
        //  System.out.println("死鬼，来人了，开始接客吧.");
    }

}