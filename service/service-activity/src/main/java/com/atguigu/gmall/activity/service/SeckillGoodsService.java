package com.atguigu.gmall.activity.service;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.activity.SeckillGoods;
import com.atguigu.gmall.model.activity.UserRecode;

import java.util.List;

/**
 * author:atGuiGu-mqx
 * date:2022/9/20 14:09
 * 描述：
 **/
public interface SeckillGoodsService {
    /**
     * 查询所有的秒杀列表
     * @return
     */
    List<SeckillGoods> findAll();

    /**
     * 根据skuId 查看商品详情
     * @param skuId
     * @return
     */
    SeckillGoods getSeckillGoods(Long skuId);

    /**
     * 预下单
     * @param skuId
     * @param skuIdStr
     * @param userId
     * @return
     */
    Result seckillOrder(Long skuId, String skuIdStr, String userId);

    /**
     * 真正用下单
     * @param userRecode
     */
    void seckillUserOrder(UserRecode userRecode);

    /**
     * 更新秒杀库存
     * @param skuId
     */
    void updateStock(Long skuId);

    /**
     * 检查订单状态
     * @param skuId
     * @param userId
     * @return
     */
    Result checkOrder(Long skuId, String userId);
}
