package com.atguigu.gmall.payment.service;

/**
 * author:atGuiGu-mqx
 * date:2022/9/17 11:49
 * 描述：
 **/
public interface AlipayService {

    //  生成二维码！
    String createAliPay(Long orderId);

    //  退款接口
    Boolean refund(Long orderId);

    //  关闭交易接口。
    Boolean closePay(Long orderId);

    //  检查交易记录
    Boolean checkPayment(Long orderId);
}
