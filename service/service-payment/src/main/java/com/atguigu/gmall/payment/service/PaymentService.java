package com.atguigu.gmall.payment.service;

import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.model.payment.PaymentInfo;

import java.util.Map;

/**
 * author:atGuiGu-mqx
 * date:2022/9/17 10:52
 * 描述：
 **/
public interface PaymentService {

    //  http://api.gmall.com/api/payment/alipay/submit/78

    /**
     *  保存交易记录：
     * @param orderInfo 订单信息
     * @param paymentType   支付类型
     */
    void savePaymentInfo(OrderInfo orderInfo,String paymentType);

    /**
     * 获取交易记录
     * @param outTradeNo
     * @param paymentType
     * @return
     */
    PaymentInfo getPaymentInfo(String outTradeNo, String paymentType);

    /**
     * 更新交易记录.
     * @param outTradeNo
     * @param paymentType
     * @param paramsMap
     */
    void paySuccess(String outTradeNo, String paymentType, Map<String, String> paramsMap);

    /**
     * 更新交易记录状态.
     * @param outTradeNo
     * @param paymentType
     * @param paymentInfo
     */
    void updatePaymentInfoStatus(String outTradeNo, String paymentType, PaymentInfo paymentInfo);

    /**
     * 关闭交易记录.
     * @param orderId
     */
    void closePayment(Long orderId);
}
