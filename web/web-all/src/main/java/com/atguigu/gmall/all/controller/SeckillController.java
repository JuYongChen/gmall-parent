package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.activity.client.ActivityFeignClient;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * author:atGuiGu-mqx
 * date:2022/9/20 14:25
 * 描述：
 **/
@Controller
public class SeckillController {

    @Autowired
    private ActivityFeignClient activityFeignClient;

    //  http://sph-activity.atguigu.cn/seckill.html
    @GetMapping("seckill.html")
    public String seckill(Model model){
        //  list
        Result result = activityFeignClient.findAll();
        model.addAttribute("list",result.getData());

        //  秒杀首页
        return "seckill/index";
    }

    //  http://activity.gmall.com/seckill/28.html
    @GetMapping("seckill/{skuId}.html")
    public String seckillItem(@PathVariable Long skuId,Model model){
        //  获取数据
        Result result = this.activityFeignClient.getSeckillGoods(skuId);
        model.addAttribute("item",result.getData());
        //  返回详情页面.
        return "seckill/item";
    }


    //  http://activity.gmall.com/seckill/queue.html?skuId=28&skuIdStr=c81e728d9d4c2f636f067f89cc14862c
    @GetMapping("seckill/queue.html")
    public String seckillQueue(HttpServletRequest request){
        //  获取skuId 以及抢购码
        request.setAttribute("skuId",request.getParameter("skuId"));
        request.setAttribute("skuIdStr",request.getParameter("skuIdStr"));
        //  返回排队页面
        return "seckill/queue";
    }

    //  去下单：<a href="/seckill/trade.html" target="_blank">去下单</a>
    @GetMapping("/seckill/trade.html")
    public String seckillTrade(Model model){
        //  userAddressList， detailArrayList， totalNum， totalAmount
        //  远程调用：
        Result<Map> result = this.activityFeignClient.seckillTrade();
        //  判断
        if (result.isOk()){
            //  保存数据
            model.addAllAttributes(result.getData());
            return "seckill/trade";
        }else {
            model.addAttribute("message","订单结算页展示失败.");
            return "seckill/fail";
        }
    }



}